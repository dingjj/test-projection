// UTM WGS84 North
export const UTM_WKIDs_North = {
  start: 32601,
  stop: 32660,
};
// GK CGCS2000 ZONE
export const CGCS2000_WKIDs = {
  start: 4491,
  stop: 4501,
}
